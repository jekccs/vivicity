<?php

namespace Fyb\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160229112338 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sylius_product ADD renewed_product_id INT DEFAULT NULL, ADD cost_type VARCHAR(32) NOT NULL, ADD listing_type VARCHAR(32) DEFAULT NULL, ADD price_type VARCHAR(32) DEFAULT NULL, ADD auto_renew TINYINT(1) DEFAULT NULL, ADD renewed_at DATETIME DEFAULT NULL, ADD valid_until DATETIME DEFAULT NULL, ADD validity_renewal VARCHAR(32) DEFAULT NULL, ADD visible TINYINT(1) DEFAULT NULL, ADD number_of_days INT DEFAULT NULL, ADD maximum_time INT DEFAULT NULL, ADD start_date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE sylius_product ADD CONSTRAINT FK_677B9B74BEF40B31 FOREIGN KEY (renewed_product_id) REFERENCES sylius_product (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_677B9B74BEF40B31 ON sylius_product (renewed_product_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sylius_product DROP FOREIGN KEY FK_677B9B74BEF40B31');
        $this->addSql('DROP INDEX IDX_677B9B74BEF40B31 ON sylius_product');
        $this->addSql('ALTER TABLE sylius_product DROP renewed_product_id, DROP cost_type, DROP listing_type, DROP price_type, DROP auto_renew, DROP renewed_at, DROP valid_until, DROP validity_renewal, DROP visible, DROP number_of_days, DROP maximum_time, DROP start_date');
    }
}
