<?php

namespace Fyb\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160311163649 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sylius_taxon DROP INDEX UNIQ_CFD811CAF61E42C6, ADD INDEX IDX_CFD811CAF61E42C6 (listing_archetype_id)');
        $this->addSql('ALTER TABLE sylius_taxon DROP INDEX UNIQ_CFD811CAC4842DBA, ADD INDEX IDX_CFD811CAC4842DBA (profile_archetype_id)');
        $this->addSql('DROP INDEX fulltext_search_idx ON sylius_search_index');
        $this->addSql('CREATE INDEX fulltext_search_idx ON sylius_search_index (item_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX fulltext_search_idx ON sylius_search_index');
        $this->addSql('CREATE FULLTEXT INDEX fulltext_search_idx ON sylius_search_index (value)');
        $this->addSql('ALTER TABLE sylius_taxon DROP INDEX IDX_CFD811CAF61E42C6, ADD UNIQUE INDEX UNIQ_CFD811CAF61E42C6 (listing_archetype_id)');
        $this->addSql('ALTER TABLE sylius_taxon DROP INDEX IDX_CFD811CAC4842DBA, ADD UNIQUE INDEX UNIQ_CFD811CAC4842DBA (profile_archetype_id)');
    }
}
