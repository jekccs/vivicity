<?php

namespace Fyb\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160312094519 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sylius_product DROP FOREIGN KEY FK_677B9B74BEF40B31');
        $this->addSql('DROP INDEX IDX_677B9B74BEF40B31 ON sylius_product');
        $this->addSql('ALTER TABLE sylius_product DROP validity_renewal, CHANGE info_type info_type VARCHAR(32) NOT NULL COMMENT \'Product Type. Possible values: listing, profile.\', CHANGE cost_type cost_type VARCHAR(32) NOT NULL COMMENT \'Product CostType. Possible values: normal, display, advertise.\', CHANGE listing_type listing_type VARCHAR(32) DEFAULT NULL COMMENT \'Product ListingType. Possible values: product, classified, inquiry, personal_details.\', CHANGE price_type price_type VARCHAR(32) DEFAULT NULL COMMENT \'
                            Product PriceType, only for CostType - normal, ListingType - product. Possible values: regular, bid.
                        \', CHANGE renewed_product_id display_cost_product_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE sylius_product ADD CONSTRAINT FK_677B9B74212548F5 FOREIGN KEY (display_cost_product_id) REFERENCES sylius_product (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_677B9B74212548F5 ON sylius_product (display_cost_product_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sylius_product DROP FOREIGN KEY FK_677B9B74212548F5');
        $this->addSql('DROP INDEX IDX_677B9B74212548F5 ON sylius_product');
        $this->addSql('ALTER TABLE sylius_product ADD validity_renewal VARCHAR(32) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE info_type info_type VARCHAR(32) NOT NULL COLLATE utf8_unicode_ci, CHANGE cost_type cost_type VARCHAR(32) NOT NULL COLLATE utf8_unicode_ci, CHANGE listing_type listing_type VARCHAR(32) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE price_type price_type VARCHAR(32) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE display_cost_product_id renewed_product_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE sylius_product ADD CONSTRAINT FK_677B9B74BEF40B31 FOREIGN KEY (renewed_product_id) REFERENCES sylius_product (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_677B9B74BEF40B31 ON sylius_product (renewed_product_id)');
    }
}
