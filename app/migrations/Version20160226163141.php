<?php

namespace Fyb\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160226163141 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE fyb_store_taxon (store_id INT NOT NULL, taxon_id INT NOT NULL, INDEX IDX_9FB0B0C5B092A811 (store_id), INDEX IDX_9FB0B0C5DE13F470 (taxon_id), PRIMARY KEY(store_id, taxon_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fyb_cost_product (id INT AUTO_INCREMENT NOT NULL, price INT DEFAULT NULL, number_of_days INT DEFAULT NULL, maximum_time INT DEFAULT NULL, start_date DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE fyb_store_taxon ADD CONSTRAINT FK_9FB0B0C5B092A811 FOREIGN KEY (store_id) REFERENCES fyb_store (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE fyb_store_taxon ADD CONSTRAINT FK_9FB0B0C5DE13F470 FOREIGN KEY (taxon_id) REFERENCES sylius_taxon (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE fyb_store_taxon');
        $this->addSql('DROP TABLE fyb_cost_product');
    }
}
