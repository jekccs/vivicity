<?php

namespace Fyb\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160225174716 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sylius_taxon DROP FOREIGN KEY FK_CFD811CAAC801922');
        $this->addSql('ALTER TABLE sylius_taxon DROP FOREIGN KEY FK_CFD811CAFE884EAC');
        $this->addSql('DROP INDEX UNIQ_CFD811CAFE884EAC ON sylius_taxon');
        $this->addSql('DROP INDEX UNIQ_CFD811CAAC801922 ON sylius_taxon');
        $this->addSql('ALTER TABLE sylius_taxon ADD listing_archetype_id INT DEFAULT NULL, ADD profile_archetype_id INT DEFAULT NULL, DROP service_archetype_id, DROP product_archetype_id');
        $this->addSql('ALTER TABLE sylius_taxon ADD CONSTRAINT FK_CFD811CAF61E42C6 FOREIGN KEY (listing_archetype_id) REFERENCES sylius_archetype (id)');
        $this->addSql('ALTER TABLE sylius_taxon ADD CONSTRAINT FK_CFD811CAC4842DBA FOREIGN KEY (profile_archetype_id) REFERENCES sylius_archetype (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_CFD811CAF61E42C6 ON sylius_taxon (listing_archetype_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_CFD811CAC4842DBA ON sylius_taxon (profile_archetype_id)');
        $this->addSql('DROP INDEX fulltext_search_idx ON sylius_search_index');
        $this->addSql('CREATE INDEX fulltext_search_idx ON sylius_search_index (item_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX fulltext_search_idx ON sylius_search_index');
        $this->addSql('CREATE FULLTEXT INDEX fulltext_search_idx ON sylius_search_index (value)');
        $this->addSql('ALTER TABLE sylius_taxon DROP FOREIGN KEY FK_CFD811CAF61E42C6');
        $this->addSql('ALTER TABLE sylius_taxon DROP FOREIGN KEY FK_CFD811CAC4842DBA');
        $this->addSql('DROP INDEX UNIQ_CFD811CAF61E42C6 ON sylius_taxon');
        $this->addSql('DROP INDEX UNIQ_CFD811CAC4842DBA ON sylius_taxon');
        $this->addSql('ALTER TABLE sylius_taxon ADD service_archetype_id INT DEFAULT NULL, ADD product_archetype_id INT DEFAULT NULL, DROP listing_archetype_id, DROP profile_archetype_id');
        $this->addSql('ALTER TABLE sylius_taxon ADD CONSTRAINT FK_CFD811CAAC801922 FOREIGN KEY (service_archetype_id) REFERENCES sylius_archetype (id)');
        $this->addSql('ALTER TABLE sylius_taxon ADD CONSTRAINT FK_CFD811CAFE884EAC FOREIGN KEY (product_archetype_id) REFERENCES sylius_archetype (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_CFD811CAFE884EAC ON sylius_taxon (product_archetype_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_CFD811CAAC801922 ON sylius_taxon (service_archetype_id)');
    }
}
