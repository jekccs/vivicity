<?php

namespace Fyb\Component\Core\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Fyb\Component\Store\Model\Store;
use Sylius\Component\Core\Model\Taxon as BaseTaxon;
use Fyb\Component\Attribute\Model\Archetype;

class Taxon extends BaseTaxon
{
    /** @var  Store[]|ArrayCollection */
    protected $stores;
    /** @var  Archetype */
    protected $listingArchetype;
    /** @var  Archetype */
    protected $profileArchetype;

    /**
     * Taxon constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->stores = new ArrayCollection();
    }

    /**
     * @return Archetype
     */
    public function getListingArchetype()
    {
        return $this->listingArchetype;
    }

    /**
     * @param Archetype $listingArchetype
     */
    public function setListingArchetype($listingArchetype)
    {
        $this->listingArchetype = $listingArchetype;
    }

    /**
     * @return Archetype
     */
    public function getProfileArchetype()
    {
        return $this->profileArchetype;
    }

    /**
     * @param Archetype $profileArchetype
     */
    public function setProfileArchetype($profileArchetype)
    {
        $this->profileArchetype = $profileArchetype;
    }

    /**
     * @return ArrayCollection|Store[]
     */
    public function getStores()
    {
        return $this->stores;
    }

    /**
     * @param ArrayCollection|Store[] $stores
     */
    public function setStores($stores)
    {
        $this->stores = $stores;
    }

    /**
     * {@inheritdoc}
     */
    public function hasStores()
    {
        return !$this->stores->isEmpty();
    }

    /**
     * {@inheritdoc}
     */
    public function addStore(Store $store)
    {
        if (!$this->hasStore($store)) {
            $this->stores->add($store);
            $store->addTaxon($this);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function removeStore(Store $store)
    {
        if ($this->hasStore($store)) {
            $this->stores->removeElement($store);
            $store->removeTaxon($this);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function hasStore(Store $store)
    {
        return $this->stores->contains($store);
    }
}
