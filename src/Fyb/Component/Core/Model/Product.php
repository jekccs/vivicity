<?php

namespace Fyb\Component\Core\Model;

use Fyb\Component\Store\Model\Store;
use Sylius\Component\Core\Model\Product as BaseProduct;

class Product extends BaseProduct
{
    /** Info Type */
    const LISTING_TYPE = 'listing';
    const PROFILE_TYPE  = 'profile';
    /** Cost Type */
    const NORMAL_TYPE  = 'normal';
    const DISPLAY_TYPE  = 'display';
    const ADVERTISE_TYPE  = 'advertise';
    /** Price Type */
    const REGULAR_TYPE  = 'regular';
    const BID_TYPE  = 'bid';
    /** Listing Type */
    const PRODUCT_TYPE  = 'product';
    const CLASSIFIED_TYPE  = 'classified';
    const INQUIRY_TYPE  = 'inquiry';
    const PERSONAL_DETAILS_TYPE  = 'personal_details';

    /** @var  string */
    protected $infoType = self::LISTING_TYPE;
    /** @var string  */
    protected $costType = self::NORMAL_TYPE;
    /** @var string  */
    protected $listingType = self::PRODUCT_TYPE;
    /** @var string  */
    protected $priceType = self::REGULAR_TYPE;
    /** @var  bool */
    protected $autoRenew;
    /** @var  \DateTime */
    protected $renewedAt;
    /** @var  Product */
    protected $displayCostProduct;
    /** @var  \DateTime */
    protected $validUntil;
    /** @var  bool */
    protected $visible;
    /** @var  integer */
    protected $numberOfDays;
    /** @var  integer */
    protected $maximumTime;
    /** @var  \DateTime */
    protected $startDate;
    /** @var  Store */
    protected $store;

    /**
     * @return Store
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * @param Store $store
     */
    public function setStore($store)
    {
        $this->store = $store;
    }

    /**
     * @return string
     */
    public function getInfoType()
    {
        return $this->infoType;
    }

    /**
     * @param string $infoType
     */
    public function setInfoType($infoType)
    {
        $this->infoType = $infoType;
    }

    /**
     * @return string
     */
    public function getCostType()
    {
        return $this->costType;
    }

    /**
     * @param string $costType
     */
    public function setCostType($costType)
    {
        $this->costType = $costType;
    }

    /**
     * @return string
     */
    public function getListingType()
    {
        return $this->listingType;
    }

    /**
     * @param string $listingType
     */
    public function setListingType($listingType)
    {
        $this->listingType = $listingType;
    }

    /**
     * @return string
     */
    public function getPriceType()
    {
        return $this->priceType;
    }

    /**
     * @param string $priceType
     */
    public function setPriceType($priceType)
    {
        $this->priceType = $priceType;
    }

    /**
     * @return boolean
     */
    public function isAutoRenew()
    {
        return $this->autoRenew;
    }

    /**
     * @param boolean $autoRenew
     */
    public function setAutoRenew($autoRenew)
    {
        $this->autoRenew = $autoRenew;
    }

    /**
     * @return \DateTime
     */
    public function getRenewedAt()
    {
        return $this->renewedAt;
    }

    /**
     * @param \DateTime $renewedAt
     */
    public function setRenewedAt($renewedAt)
    {
        $this->renewedAt = $renewedAt;
    }

    /**
     * @return Product
     */
    public function getDisplayCostProduct()
    {
        return $this->displayCostProduct;
    }

    /**
     * @param Product $displayCostProduct
     */
    public function setDisplayCostProduct($displayCostProduct)
    {
        $this->displayCostProduct = $displayCostProduct;
    }

    /**
     * @return \DateTime
     */
    public function getValidUntil()
    {
        return $this->validUntil;
    }

    /**
     * @param \DateTime $validUntil
     */
    public function setValidUntil($validUntil)
    {
        $this->validUntil = $validUntil;
    }

    /**
     * @return boolean
     */
    public function isVisible()
    {
        return $this->visible;
    }

    /**
     * @param boolean $visible
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
    }

    /**
     * @return int
     */
    public function getNumberOfDays()
    {
        return $this->numberOfDays;
    }

    /**
     * @param int $numberOfDays
     */
    public function setNumberOfDays($numberOfDays)
    {
        $this->numberOfDays = $numberOfDays;
    }

    /**
     * @return int
     */
    public function getMaximumTime()
    {
        return $this->maximumTime;
    }

    /**
     * @param int $maximumTime
     */
    public function setMaximumTime($maximumTime)
    {
        $this->maximumTime = $maximumTime;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param \DateTime $startDate
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @return array
     */
    public static function getInfoTypeLabels()
    {
        return array(
            self::LISTING_TYPE => 'Listing',
            self::PROFILE_TYPE => 'Profile',
        );
    }

    /**
     * @return array
     */
    public static function getCostTypeLabels()
    {
        return array(
            self::NORMAL_TYPE => 'Normal',
            self::DISPLAY_TYPE => 'Display',
            self::ADVERTISE_TYPE => 'Advertise',
        );
    }

    /**
     * @return array
     */
    public static function getPriceTypeLabels()
    {
        return array(
            self::REGULAR_TYPE => 'Regular',
            self::BID_TYPE => 'Bid',
        );
    }

    /**
     * @return array
     */
    public static function getListingTypeLabels()
    {
        return array(
            self::PRODUCT_TYPE => 'Product',
            self::CLASSIFIED_TYPE => 'Classified',
            self::INQUIRY_TYPE => 'Inquiry',
            self::PERSONAL_DETAILS_TYPE => 'Personal Details',
        );
    }
}
