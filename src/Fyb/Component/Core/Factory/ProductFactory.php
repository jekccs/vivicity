<?php

namespace Fyb\Component\Core\Factory;

use Fyb\Component\Core\Model\Product;
use Fyb\Component\Core\Model\Taxon;
use Sylius\Component\Product\Factory\ProductFactoryInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ProductFactory implements FactoryInterface
{
    /**
     * @var ProductFactoryInterface
     */
    private $factory;

    /**
     * @var RepositoryInterface
     */
    private $taxonRepository;

    /**
     * @var UserInterface
     */
    private $user;

    /**
     * @param ProductFactoryInterface $factory
     * @param RepositoryInterface     $taxonRepository
     * @param TokenStorageInterface   $token
     * @param RepositoryInterface     $productRepository
     */
    public function __construct(
        ProductFactoryInterface $factory,
        RepositoryInterface $taxonRepository,
        TokenStorageInterface $token,
        RepositoryInterface $productRepository
    ) {
        $this->factory = $factory;
        $this->taxonRepository = $taxonRepository;
        $this->user = $token->getToken()->getUser();
        $this->productRepository = $productRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function createNew()
    {
        return $this->factory->createNew();
    }

    /**
     * {@inheritdoc}
     */
    public function createFromArchetype($archetypeCode)
    {
        return $this->factory->createFromArchetype($archetypeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function createProfileFromTaxon($taxonCode)
    {
        /** @var $taxon Taxon */
        if (null === $taxon = $this->taxonRepository->findOneBy(['code' => $taxonCode])) {
            throw new \InvalidArgumentException(sprintf('Requested taxon does not exist with code "%s".', $taxonCode));
        }

        if ($archetype = $taxon->getProfileArchetype()) {
            $product = $this->createFromArchetype($archetype->getCode());
        } else {
            $product = $this->createNew();
        }
        if ($this->user->getStore()) {
            $product->setStore($this->user->getStore());
            $this->user->getStore()->addTaxon($taxon);
        }

        $product->setMainTaxon($taxon);
        $product->addTaxon($taxon);

        return $product;
    }

    /**
     * {@inheritdoc}
     */
    public function createListingFromTaxon($taxonCode)
    {
        /** @var $taxon Taxon */
        if (null === $taxon = $this->taxonRepository->findOneBy(['code' => $taxonCode])) {
            throw new \InvalidArgumentException(sprintf('Requested taxon does not exist with code "%s".', $taxonCode));
        }

        if ($archetype = $taxon->getListingArchetype()) {
            $product = $this->createFromArchetype($archetype->getCode());
        } else {
            $product = $this->createNew();
        }

        if ($this->user->getStore()) {
            $product->setStore($this->user->getStore());
        }
        $criteria = array(
            'infoType' => Product::LISTING_TYPE,
            'costType' => Product::DISPLAY_TYPE,
        );
        if (!$displayCostProduct = $this->productRepository->getDisplayCostProductByTaxon($taxon, $criteria)) {
            throw new NotFoundHttpException('Associate Cost not found');
        }

        $product->setDisplayCostProduct($displayCostProduct);
        $product->setRenewedAt(new \DateTime());
        $interval = 'P'.(int) $displayCostProduct->getMaximumTime().'D';
        $product->setValidUntil((new \DateTime())->add(new \DateInterval($interval)));
        $product->setMainTaxon($taxon);
        $product->addTaxon($taxon);

        return $product;
    }
}
