<?php

namespace Fyb\Component\Core\Factory;

use Sylius\Component\Resource\Factory\FactoryInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;

class CustomerFactory implements FactoryInterface
{
    /**
     * @var FactoryInterface
     */
    private $factory;

    /**
     * @var RepositoryInterface
     */
    private $roleRepository;

    /**
     * @var FactoryInterface
     */
    private $userFactory;

    /**
     * @param FactoryInterface    $factory
     * @param RepositoryInterface $repository
     * @param FactoryInterface    $userFactory
     */
    public function __construct(FactoryInterface $factory, RepositoryInterface $repository, FactoryInterface $userFactory)
    {
        $this->factory = $factory;
        $this->roleRepository = $repository;
        $this->userFactory = $userFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function createNew()
    {
        $customer = $this->factory->createNew();

        $user = $this->userFactory->createNew();
        $user->addAuthorizationRole($this->roleRepository->findOneBy(array('code' => 'listing_manager')));
        $user->setCustomer($customer);

        return $customer;
    }
}
