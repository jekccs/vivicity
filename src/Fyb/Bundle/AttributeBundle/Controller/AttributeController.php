<?php

namespace Fyb\Bundle\AttributeBundle\Controller;

use Sylius\Bundle\AttributeBundle\Controller\AttributeController as BaseAttributeController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Fyb\Component\Attribute\Model\Archetype;
use Fyb\Component\Attribute\Model\Attribute;
use Fyb\Component\Attribute\Model\AttributeWidget;

class AttributeController extends BaseAttributeController
{
    /**
     * {@inheritdoc}
     */
    public function renderAttributeValueFormsAction(Request $request)
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $attributeRepository = $this->get($this->metadata->getServiceId('repository'));
        $forms = [];

        $choices = $request->query->get(sprintf('sylius_%s_choice', $this->metadata->getName()), []);

        $attributes = $attributeRepository->findBy(['id' => $choices]);
        foreach ($attributes as $attribute) {
            $attributeForm = sprintf('sylius_attribute_type_%s_%s', $attribute->getType(), $attribute->getBackendWidget());

            $options = ['label' => $attribute->getName()];

            $form = $this->get('form.factory')->createNamed('value', $attributeForm, null, $options);
            $forms[$attribute->getId()] = $form->createView();
        }

        return $this->render('FybAttributeBundle::attributeValueForms.html.twig', [
            'forms' => $forms,
            'count' => $request->query->get('count'),
            'metadata' => $this->metadata,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function renderAttributesAction(Request $request)
    {
        $form = $this->get('form.factory')->create(
            sprintf('sylius_%s_choice', $this->metadata->getName()),
            null,
            [
                'expanded' => true,
                'multiple' => true,
            ]
        );

        $view = $request->get('widget', 'SyliusAttributeBundle::attributeChoice.html.twig');

        return $this->render($view, ['form' => $form->createView()]);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function renderAttributeWidgetFormsAction(Request $request)
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $attributeRepository = $this->get($this->metadata->getServiceId('repository'));

        /** @var Archetype $archetype */
        $archetype = $this->get('sylius.repository.product_archetype')->find($request->query->get('archetype_parent', 0));
        $forms = [];
        $choices = ($request->query->has('sylius_product_attribute_choice')) ? $request->query->get('sylius_product_attribute_choice') : 0;
        /** @var  $attributes Attribute[] */
        $attributes = $attributeRepository->findBy(['id' => $choices]);

        if ($archetype && !$choices) {
            $attributes  = array_merge($archetype->getAttributes()->toArray(), $attributes);
        }
        foreach ($attributes as $attribute) {
            $attributeWidget = $this->getAttributeWidget($attribute, $archetype);
            $options = array(
                'label' => $attribute->getName(),
            );

            $form = $this->get('form.factory')->createNamed('widgets', 'sylius_product_archetype_attribute_widget', $attributeWidget, $options);
            $forms[$attribute->getId()] = $form->createView();
        }

        return $this->render('FybAttributeBundle::attributeWidgetForms.html.twig', array('forms' => $forms, 'count' => $request->query->get('count')));
    }

    /**
     * @param Attribute $attribute
     * @param Archetype|null $parentArchetype
     * @return AttributeWidget
     */
    private function getAttributeWidget(Attribute $attribute, Archetype $parentArchetype = null)
    {
        $attributeWidget = new AttributeWidget();
        $attributeWidget->setAttribute($attribute);
        $repository = $this->get('fyb.repository.attribte_widget');

        /** @var AttributeWidget $parentAttributeWidget */
        if ($parentAttributeWidget = $repository->getAttributeWidget($attribute, $parentArchetype)) {
            $attributeWidget->setFrontendWidget($parentAttributeWidget->getFrontendWidget());
            $attributeWidget->setBackendWidget($parentAttributeWidget->getBackendWidget());
        } else {
            $attributeWidget->setFrontendWidget($attribute->getFrontendWidget());
            $attributeWidget->setBackendWidget($attribute->getBackendWidget());
        }

        return $attributeWidget;
    }
}
