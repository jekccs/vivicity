<?php

namespace Fyb\Bundle\CoreBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use Fyb\Bundle\CoreBundle\Doctrine\ORM\ProductRepository;
use Fyb\Component\Core\Model\Product;
use Sylius\Bundle\ResourceBundle\Event\ResourceControllerEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AddCostProductListener
{
    /**
     * @var ProductRepository
     */
    protected $repository;

    /**
     * @var EntityManagerInterface
     */
    protected $manager;

    /**
     * Constructor.
     *
     * @param ProductRepository      $repository
     * @param EntityManagerInterface $manager
     */
    public function __construct(ProductRepository $repository, EntityManagerInterface $manager)
    {
        $this->repository = $repository;
        $this->manager = $manager;
    }

    /**
     * @param ResourceControllerEvent $event
     */
    public function prePersist(ResourceControllerEvent $event)
    {
        $item = $event->getSubject();
        $criteria = array(
            'infoType' => $item->getInfoType(),
        );
        /** @var Product $item */
        if (Product::NORMAL_TYPE === $item->getCostType()) {
            $criteria['costType'] = Product::DISPLAY_TYPE;
            if ($displayCostProduct = $this->repository->getDisplayCostProductByTaxon($item->getMainTaxon(), $criteria)) {
                $item->setDisplayCostProduct($displayCostProduct);
                $item->setVisible(false);
            } else {
                throw new NotFoundHttpException('Associate Cost not found');
            }
        } else {
            foreach ($item->getTaxons() as $taxon) {
                if ($displayCostProduct = $this->repository->getDisplayCostProductByTaxon($taxon, $criteria)) {
                    $displayCostProduct->removeTaxon($taxon);
                    $this->manager->persist($displayCostProduct);
                }
            }
            $item->setListingType(null);
            $item->setPriceType(null);
        }
    }
}
