<?php

namespace Fyb\Bundle\CoreBundle\Form\Type;

use Fyb\Bundle\CoreBundle\Form\EventListener\BuildCostTypeFormSubscriber;
use Sylius\Bundle\CoreBundle\Form\Type\ProductType as BaseProductType;
use Symfony\Component\Form\FormBuilderInterface;
use Fyb\Component\Attribute\Model\Archetype;
use Fyb\Component\Core\Model\Product;
use Fyb\Component\Core\Model\Taxon;

class ProductType extends BaseProductType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        /** @var Product $product */
        $product = $options['data'];
        $archetype = $product->getArchetype();
        $builder
            ->add('attributes', 'collection', array(
                'required'     => false,
                'type'         => 'sylius_product_attribute_value',
                'prototype' => false,
                'allow_add'    => true,
                'allow_delete' => true,
                'by_reference' => false,
                'options' => array(
                    'archetype' => $archetype,
                ),
            ))
            ->add('taxons', 'sylius_taxon_selection', array(
                'archetype' => $archetype,
            ))
            ->add('mainTaxon', 'sylius_taxon_choice', array(
                'label' => 'sylius.form.product.main_taxon',
                'filter' => function (Taxon $taxon) use ($archetype) {
                    if ($archetype instanceof Archetype) {
                        $getArchetype = sprintf('get%sArchetype', ucfirst($archetype->getType()));

                        return $taxon->$getArchetype() === $archetype;
                    }

                    return true;
                },
            ))
            ->add('infoType', 'choice', array(
                'label'   => 'Info Type',
                'choices' => Product::getInfoTypeLabels(),
                'empty_data' => Product::LISTING_TYPE,
                'expanded' => true,
            ))
            ->add('listingType', 'choice', array(
                'label'   => 'Listing Type',
                'choices' => Product::getListingTypeLabels(),
                'empty_data' => Product::PRODUCT_TYPE,
            ))
            ->add('priceType', 'choice', array(
                'label'   => 'Price Type',
                'choices' => Product::getPriceTypeLabels(),
                'empty_data' => Product::REGULAR_TYPE,
                'expanded' => true,
            ))
            ->add('costType', 'choice', array(
                'label'   => 'Cost Type',
                'choices' => Product::getCostTypeLabels(),
                'empty_data' => Product::NORMAL_TYPE,
                'expanded' => true,
            ))
            ->addEventSubscriber(new BuildCostTypeFormSubscriber($builder->getFormFactory()))
        ;
    }
}
