<?php

namespace Fyb\Bundle\CoreBundle\Form\Type;

use Sylius\Bundle\CoreBundle\Form\Type\ProductType as BaseProductType;
use Symfony\Component\Form\FormBuilderInterface;
use Fyb\Component\Attribute\Model\Archetype;
use Fyb\Component\Core\Model\Product;
use Fyb\Component\Core\Model\Taxon;

class ProfileType extends ProductType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('infoType', 'hidden', array(
                'data' => Product::PROFILE_TYPE,
            ))
            ->add('costType', 'hidden', array(
                'data' => Product::NORMAL_TYPE,
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'fyb_profile';
    }
}
