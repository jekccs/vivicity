<?php

namespace Fyb\Bundle\CoreBundle\Form\Type;

use Fyb\Bundle\CoreBundle\Form\EventListener\BuildCostTypeFormSubscriber;
use Sylius\Bundle\CoreBundle\Form\Type\ProductType as BaseProductType;
use Symfony\Component\Form\FormBuilderInterface;
use Fyb\Component\Attribute\Model\Archetype;
use Fyb\Component\Core\Model\Product;
use Fyb\Component\Core\Model\Taxon;

class ListingType extends ProductType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('infoType', 'hidden', array(
                'data' => Product::LISTING_TYPE,
            ))
            ->add('costType', 'hidden', array(
                'data' => Product::NORMAL_TYPE,
            ))
            ->add('listingType', 'choice', array(
                'label'   => 'Listing Type',
                'choices' => Product::getListingTypeLabels(),
                'empty_data' => Product::PRODUCT_TYPE,
            ))
            ->add('priceType', 'choice', array(
                'label'   => 'Price Type',
                'choices' => Product::getPriceTypeLabels(),
                'empty_data' => Product::REGULAR_TYPE,
                'expanded' => true,
            ))
            ->addEventSubscriber(new BuildCostTypeFormSubscriber($builder->getFormFactory()))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'fyb_listing';
    }
}
