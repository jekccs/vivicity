<?php

namespace Fyb\Bundle\CoreBundle\Form\EventListener;

use Fyb\Component\Core\Model\Product;
use Sylius\Component\Resource\Exception\UnexpectedTypeException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;

class BuildCostTypeFormSubscriber implements EventSubscriberInterface
{
    /**
     * @var FormFactoryInterface
     */
    private $factory;

    /**
     * @param FormFactoryInterface $factory
     */
    public function __construct(FormFactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_SUBMIT   => 'preSubmit',
        );
    }

    /**
     * {@inheritdoc}
     */
    public function preSetData(FormEvent $event)
    {
        $product = $event->getData();

        if (null === $product) {
            return;
        }

        if (!$product instanceof Product) {
            throw new UnexpectedTypeException($product, Product::class);
        }

        $method = sprintf('add%sFields', ucfirst($product->getCostType()));

        $this->$method($event->getForm());
    }

    /**
     * {@inheritdoc}
     */
    public function preSubmit(FormEvent $event)
    {
        $data = $event->getData();

        if (empty($data) || !array_key_exists('costType', $data)) {
            return;
        }

        $method = sprintf('add%sFields', ucfirst($data['costType']));

        $this->$method($event->getForm());
    }

    /**
     * Add cost type fields to the form.
     *
     * @param FormInterface $form
     */
    public function addDisplayFields(FormInterface $form)
    {
        $form
            ->add('numberOfDays', 'number', array(
                'empty_data' => 30,
            ))
            ->add('maximumTime', 'number', array(
                'empty_data' => 90,
            ))
            ->add('startDate', 'datetime', array(
                'empty_data' => new \DateTime(),
                'date_format' => 'y-M-d',
                'date_widget' => 'choice',
                'time_widget' => 'text',
            ))
        ;
    }
    /**
     * Add cost type fields to the form.
     *
     * @param FormInterface $form
     */
    public function addAdvertiseFields(FormInterface $form)
    {
        $this->addDisplayFields($form);
    }
    /**
     * Add cost type fields to the form.
     *
     * @param FormInterface $form
     */
    public function addNormalFields(FormInterface $form)
    {
        $form
            ->add('autoRenew', 'choice', array(
                'label'   => 'Validity Renewal',
                'choices' => ['Manual', 'Automatic'],
                'expanded' => true,
            ))
            ->add('renewedAt', 'datetime', array(
                'date_format' => 'y-M-d',
                'date_widget' => 'choice',
                'time_widget' => 'text',
            ))
            ->add('validUntil', 'datetime', array(
                'date_format' => 'y-M-d',
                'date_widget' => 'choice',
                'time_widget' => 'text',
            ))
        ;
    }
}
