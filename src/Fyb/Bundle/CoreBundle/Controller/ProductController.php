<?php

namespace Fyb\Bundle\CoreBundle\Controller;

use FOS\RestBundle\View\View;
use Fyb\Component\Core\Model\Product;
use Sylius\Bundle\CoreBundle\Controller\ProductController as BaseProductController;
use Sylius\Bundle\ResourceBundle\Controller\RequestConfiguration;
use Sylius\Component\Core\Model\TaxonInterface;
use Symfony\Component\HttpFoundation\Request;
use Pagerfanta\Pagerfanta;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ProductController extends BaseProductController
{
    /**
     * {@inheritdoc}
     */
    public function indexByTaxonIdAndStoreAction(Request $request, $id)
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);
        $taxon = $this->container->get('sylius.repository.taxon')->find($id);

        if (!isset($taxon)) {
            throw new NotFoundHttpException('Requested taxon does not exist.');
        }
        $criteria = array(
            'infoType' => Product::LISTING_TYPE,
        );
        $paginator = $this->repository->createByTaxonAndStorePaginator($taxon, $this->getUser()->getStore(), $criteria);

        return $this->renderResults($configuration, $taxon, $paginator, 'productIndex.html', $request->get('page', 1));
    }

    /**
     * {@inheritdoc}
     */
    protected function isGrantedOr403(RequestConfiguration $configuration, $permission)
    {
        if (!$configuration->hasPermission()) {
            return;
        }

        if ($this->authorizationChecker->isGranted($configuration, sprintf('fyb.listing.%s', $permission))) {
            return true;
        }
        $permission = $configuration->getPermission($permission);

        if (!$this->authorizationChecker->isGranted($configuration, $permission)) {
            throw new AccessDeniedException();
        }
    }

    /**
     * {@inheritdoc}
     */
    private function renderResults(
        RequestConfiguration $configuration,
        TaxonInterface $taxon,
        Pagerfanta $results,
        $template,
        $page,
        $facets = null,
        $facetTags = null,
        $filters = null,
        $searchTerm = null,
        $searchParam = null,
        $requestMethod = null
    ) {
        $results->setCurrentPage($page, true, true);
        $results->setMaxPerPage($configuration->getPaginationMaxPerPage());

        $view = View::create()
            ->setTemplate($configuration->getTemplate($template))
            ->setData([
                'taxon' => $taxon,
                'products' => $results,
                'facets' => $facets,
                'facetTags' => $facetTags,
                'filters' => $filters,
                'searchTerm' => $searchTerm,
                'searchParam' => $searchParam,
                'requestMethod' => $requestMethod,
            ])
        ;

        return $this->viewHandler->handle($configuration, $view);
    }
}
