<?php

namespace Fyb\Bundle\CoreBundle\Controller;

use Sylius\Bundle\ResourceBundle\Controller\RequestConfiguration;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class TaxonomyController extends ResourceController
{
    /**
     * {@inheritdoc}
     */
    protected function isGrantedOr403(RequestConfiguration $configuration, $permission)
    {
        if (!$configuration->hasPermission()) {
            return;
        }

        if ($this->authorizationChecker->isGranted($configuration, sprintf('fyb.catalog.%s', $permission))) {
            return;
        }
        $permission = $configuration->getPermission($permission);

        if (!$this->authorizationChecker->isGranted($configuration, $permission)) {
            throw new AccessDeniedException();
        }
    }
}
