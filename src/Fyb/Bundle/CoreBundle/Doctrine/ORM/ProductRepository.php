<?php

namespace Fyb\Bundle\CoreBundle\Doctrine\ORM;

use Fyb\Component\Core\Model\Product;
use Fyb\Component\Core\Model\Taxon;
use Fyb\Component\Store\Model\Store;
use Pagerfanta\Pagerfanta;
use Sylius\Component\Core\Model\ChannelInterface;
use Sylius\Bundle\CoreBundle\Doctrine\ORM\ProductRepository as BaseProductRepository;
use Sylius\Component\Core\Model\TaxonInterface;

class ProductRepository extends BaseProductRepository
{

    /**
     * {@inheritdoc}
     */
    public function createByTaxonPaginator(TaxonInterface $taxon, array $criteria = [])
    {
        $criteria = array_merge($criteria, [
            'costType' => Product::NORMAL_TYPE,
//            'infoType' => Product::LISTING_TYPE, // maybe filter by infoType too
//            'visible' => true, //enable when backend cart is done
        ]);

        return parent::createByTaxonPaginator($taxon, $criteria);
    }

    /**
     * {@inheritdoc}
     */
    public function findLatest($limit = 10, ChannelInterface $channel)
    {
        return $this->findBy(
            [
                'channels' => [$channel],
                'enabled' => true,
                'costType' => Product::NORMAL_TYPE,
//                'infoType' => Product::LISTING_TYPE, // maybe filter by infoType too
//                'visible' => true, //enable when backend cart is done
            ],
            ['createdAt' => 'desc'],
            $limit
        );
    }

    /**
     * Create paginator for products categorized under given taxon and store.
     * @param Taxon      $taxon
     * @param Store|null $store
     * @param array      $criteria
     * @return Pagerfanta
     */
    public function createByTaxonAndStorePaginator(Taxon $taxon, Store $store = null, array $criteria = array())
    {
        $queryBuilder = $this->getCollectionQueryBuilder();
        $queryBuilder
            ->innerJoin('product.taxons', 'taxon')
            ->innerJoin('product.store', 'store')
            ->andWhere($queryBuilder->expr()->orX(
                'taxon = :taxon',
                ':left < taxon.left AND taxon.right < :right'
            ))
            ->andWhere('store = :store')
            ->setParameter('taxon', $taxon)
            ->setParameter('store', $store)
            ->setParameter('left', $taxon->getLeft())
            ->setParameter('right', $taxon->getRight())
        ;

        $this->applyCriteria($queryBuilder, $criteria);

        return $this->getPaginator($queryBuilder);
    }

    /**
     * {@inheritdoc}
     */
    public function createProfileFilterPaginator($criteria = array(), $sorting = array(), $deleted = false, Store $store = null)
    {
        $criteria['infoType'] = Product::PROFILE_TYPE;

        return $this->createStoreFilterPaginator($criteria, $sorting, $deleted, $store);
    }

    /**
     * {@inheritdoc}
     */
    public function createListingFilterPaginator($criteria = array(), $sorting = array(), $deleted = false, Store $store = null)
    {
        $criteria['infoType'] = Product::LISTING_TYPE;

        return $this->createStoreFilterPaginator($criteria, $sorting, $deleted, $store);
    }

    /**
     * @param Taxon $taxon
     * @param  array $criteria
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getDisplayCostProductByTaxon(Taxon $taxon, $criteria)
    {
        $queryBuilder = $this->getQueryBuilder();
        $queryBuilder
            ->leftJoin('product.taxons', 'taxon')
            ->andWhere('taxon = :taxon')
            ->setParameter('taxon', $taxon)
        ;
        $this->applyCriteria($queryBuilder, $criteria);

        $result = $queryBuilder
            ->getQuery()
            ->getOneOrNullResult()
        ;

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    protected function createStoreFilterPaginator($criteria = array(), $sorting = array(), $deleted = false, Store $store = null)
    {
        $queryBuilder = parent::getCollectionQueryBuilder()
            ->addSelect('variant')
            ->leftJoin('product.variants', 'variant')
            ->andWhere('product.infoType = :infoType')
            ->setParameter('infoType', $criteria['infoType'])
        ;

        if (!empty($criteria['name'])) {
            $queryBuilder
                ->andWhere('translation.name LIKE :name')
                ->setParameter('name', '%'.$criteria['name'].'%')
            ;
        }
        if (!empty($criteria['sku'])) {
            $queryBuilder
                ->andWhere('variant.sku = :sku')
                ->setParameter('sku', $criteria['sku'])
            ;
        }
        if ($store) {
            $queryBuilder
                ->andWhere('product.store = :store')
                ->setParameter('store', $store)
            ;
        }

        if (empty($sorting)) {
            if (!is_array($sorting)) {
                $sorting = array();
            }
            $sorting['updatedAt'] = 'desc';
        }

        $this->applySorting($queryBuilder, $sorting);
        if ($deleted) {
            $this->_em->getFilters()->disable('softdeleteable');
            $queryBuilder->andWhere('product.deletedAt IS NOT NULL');
        }

        return $this->getPaginator($queryBuilder);
    }
}
